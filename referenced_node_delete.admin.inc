<?php

/****************************************************************
 * @file                                                        *
 * Include file for Referenced Node Delete settings page.        *
 ****************************************************************/

/**
 * Referenced node delete settings page
 * @return
 *   system_settings_form($form)
 */
function referenced_node_delete_admin(&$form_state) {

  // Get node types
  $node_types = array();
  foreach (node_get_types('types') as $key => $value) {
    $node_types[$key] = $key;
  }

  // Get reference fields
  $fields = content_fields();
  $reference_fields = array();
  foreach ($fields as $field) {
    if ($field['type'] == 'nodereference') {
      $reference_fields[$field['field_name']] = $field['widget']['label'];
    }
  }

  $form['referenced_node_delete'] = array(
    '#type' => 'fieldset',
    '#title' => t('Referenced Node Delete settings'),
    '#collapsible' => FALSE,
  );

  $form['referenced_node_delete']['referenced_node_delete_type'] = array(
    '#type' => 'select',
    '#title' => t('Node type'),
    '#description' => t("Choose the node type containing the reference field"),
    '#options' => $node_types,
    '#default_value' => variable_get('referenced_node_delete_type', $default),
  );

  $form['referenced_node_delete']['referenced_node_delete_field'] = array(
    '#type' => 'select',
    '#title' => t('Reference field'),
    '#description' => t("Choose the reference field you wish to use"),
    '#options' => $reference_fields,
    '#default_value' => variable_get('referenced_node_delete_field', $default),
  );

  return system_settings_form($form);
}


/**
 * Validate the form. Check to see if the reference field exist in
 * the chosen node type.
 */
function referenced_node_delete_admin_validate(&$form_state) {

  $type = $form_state['referenced_node_delete']['referenced_node_delete_type']['#value'];
  $field = $form_state['referenced_node_delete']['referenced_node_delete_field']['#value'];

  if (!content_fields($field, $type)) {
    form_set_error('referenced_node_delete_type', t('There is no reference field in that node type.'));
  }
}